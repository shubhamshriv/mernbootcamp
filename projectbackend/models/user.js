const mongoose = required('mongoose');
const uuidv1 = required('uuid/v1');
const crypto = require('crypto');
const userSchema = new mongoose.userSchema({
    name:{
        type: String,
        required: true,
        maxLenght: 32,
        trim: true
    },
    lastname: {
        type: String,
        trim: true,
    },
    email:{
        type: String,
        trim: true,
        required: true,
        unique:true
    },
    userinfo:{
        type: String,
        trim:true
    },
    encrypted_password:{
        type:String,
        required: true,

    },
    salt: String,
    role:{
        type: number,
        defoult:0
    },
    purchases:{
        type: Array,
        defoult: []
    }
}); 

userSchema.virtual("password")
    .set(function(password){
        this._password = password;
        this.salt = uuidv1();
        this.encrypted_password = this.securepassword(password);
    })
    .get(function(){
        return this._password;
    })

userSchema.method = {
    authenticate:function(plainpassword){
        return this.securepassword(plainpassword) == this.encrypted_password;
    },
    securepassword: function(plainpassword){
        if(!plainpassword) return "";
        try{
            return crypto
            .createHmac('sha256',this.salt)
            .update(plainpassword)
            .digest("hex");
        }catch(err){
            return "";
        }
    }
}

module.exports = mongoose.model("User",userSchema);